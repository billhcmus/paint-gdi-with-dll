﻿# Thông tin cá nhân
- MSSV: 1512557
- Họ tên: Phan Trọng Thuyên
# Cải tiến ứng dụng vẽ với DLL
# Các chức năng đã làm được
## Sử dụng load time, runtime

1. Đưa tất cả các phương thức của lớp vào dll
2. Đưa 2 hàm tạo struct file bitmap và tạo file bitmap vào dll sử dụng kỹ thuật load runtime
3. Dùng hàm LoadLibrary khi load runtime của 2 hàm bitmap

## Yêu cầu nâng cao
1. 
- Bọc tất cả các đối tượng vẽ vào các lớp model
- Sử dụng đa xạ (polymorphism) để cài đặt việc quản lý các đối tượng và vẽ hình.
- Sử dụng mẫu thiết kế prototypes để tạo ra hàng mẫu nhằm vẽ ở chế độ xem trước (preview).
2. Lưu và nạp hình

## Các luồng sự kiện chính
> Chạy chương trình, mặc định nét vẽ là nét liền, màu đen, chưa định vị màu tô
> Loại hình vẽ mặc định là đường thẳng
> Nhấn giữ chuột trái và di chuyển con trỏ chuột đến vị trí kết thúc, nhả chuột thì hình vẽ tương ứng tạo ra
> Trong quá trình di chuyển thì hình vẽ được preview
> Menu Draw cho phép lựa chọn loại hình
> Menu File cho phép tạo mới, mở file bitmap, lưu file bitmap
> Khi chạy cần có file MyDLL.dll

## Các luồng sự kiện phụ
> Vẽ không nháy
> Hình không đè lên nhau

## Yêu cầu khác
> Link repo: git@bitbucket.org:billhcmus/paint-gdi-with-dll.git

> Link video: https://youtu.be/UU0TpoySyLE

> Nền tảng build: Visual Studio 2017