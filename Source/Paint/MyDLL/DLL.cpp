﻿#include "stdafx.h"
#include "DLL.h"

namespace PaintLibrary
{

	void CShape::SetStartPos(Point startpos)
	{
		this->StartPos = startpos;
	}

	void CShape::SetEndPos(Point endpos)
	{
		this->EndPos = endpos;
	}

	void CShape::SetSizePen(int lineWidth)
	{
		this->pen->SetWidth(lineWidth);
	}

	void CShape::SetPenColor(DWORD rgb)
	{
		Color clr;
		clr.SetFromCOLORREF(rgb);
		this->pen->SetColor(clr);
	}

	// CLine define

	void CLine::Draw(Graphics* graphics)
	{
		graphics->DrawLine(this->pen, this->StartPos, this->EndPos);
	}

	CShape* CLine::Clone()
	{
		return new CLine;
	}

	void CLine::CreatePen()
	{
		this->pen = new Pen(Color(255, 0, 0, 0));
	}

	void CLine::CreateBrush()
	{
		this->brush = new SolidBrush(Color(255, 0, 0, 255));
	}

	// bitmap
	void CBMP::Init(Image* img, Point pnt)
	{
		this->im = img;
		this->point = pnt;
	}

	void CBMP::Draw(Graphics* graphics)
	{
		graphics->DrawImage(im, point.X, point.Y);
	}

	CShape* CBMP::Clone()
	{
		return NULL;
	}

	void CBMP::CreatePen()
	{
		this->pen = new Pen(Color(255, 0, 0, 0));
	}

	void CBMP::CreateBrush()
	{
		this->brush = new SolidBrush(Color(255, 0, 0, 255));
	}


	// Rectangle
	void CRectangle::Draw(Graphics* graphics)
	{
		graphics->DrawRectangle(pen, this->StartPos.X, this->StartPos.Y, this->EndPos.X - this->StartPos.X, this->EndPos.Y - this->StartPos.Y);
	}

	CShape* CRectangle::Clone()
	{
		return new CRectangle;
	}

	void CRectangle::CreatePen()
	{
		this->pen = new Pen(Color(255, 0, 0, 0));
	}

	void CRectangle::CreateBrush()
	{
		this->brush = new SolidBrush(Color(255, 0, 0, 255));
	}

	// Ellipse
	void CEllipse::Draw(Graphics* graphics)
	{
		graphics->DrawEllipse(pen, this->StartPos.X, this->StartPos.Y, this->EndPos.X - this->StartPos.X, this->EndPos.Y - this->StartPos.Y);
	}
	CShape* CEllipse::Clone()
	{
		return new CEllipse;
	}
	void CEllipse::CreatePen()
	{
		this->pen = new Pen(Color(255, 0, 0, 0));
	}

	void CEllipse::CreateBrush()
	{
		this->brush = new SolidBrush(Color(255, 0, 0, 255));
	}
}

// tạo struct file bitmap (nguồn MSDN)
PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp)
{
	BITMAP bmp;
	PBITMAPINFO pbmi;
	WORD    cClrBits;

	// Retrieve the bitmap color format, width, and height.  
	if (!GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp))
		MessageBox(hwnd, L"GetObject", L"Error", MB_OK);

	// Convert the color format to a count of bits.  
	cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel);
	if (cClrBits == 1)
		cClrBits = 1;
	else if (cClrBits <= 4)
		cClrBits = 4;
	else if (cClrBits <= 8)
		cClrBits = 8;
	else if (cClrBits <= 16)
		cClrBits = 16;
	else if (cClrBits <= 24)
		cClrBits = 24;
	else cClrBits = 32;

	// Allocate memory for the BITMAPINFO structure. (This structure  
	// contains a BITMAPINFOHEADER structure and an array of RGBQUAD  
	// data structures.)  

	if (cClrBits < 24)
		pbmi = (PBITMAPINFO)LocalAlloc(LPTR,
			sizeof(BITMAPINFOHEADER) +
			sizeof(RGBQUAD) * (1 << cClrBits));

	// There is no RGBQUAD array for these formats: 24-bit-per-pixel or 32-bit-per-pixel 

	else
		pbmi = (PBITMAPINFO)LocalAlloc(LPTR,
			sizeof(BITMAPINFOHEADER));

	// Initialize the fields in the BITMAPINFO structure.  

	pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pbmi->bmiHeader.biWidth = bmp.bmWidth;
	pbmi->bmiHeader.biHeight = bmp.bmHeight;
	pbmi->bmiHeader.biPlanes = bmp.bmPlanes;
	pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel;
	if (cClrBits < 24)
		pbmi->bmiHeader.biClrUsed = (1 << cClrBits);

	// If the bitmap is not compressed, set the BI_RGB flag.  
	pbmi->bmiHeader.biCompression = BI_RGB;

	// Compute the number of bytes in the array of color  
	// indices and store the result in biSizeImage.  
	// The width must be DWORD aligned unless the bitmap is RLE 
	// compressed. 
	pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits + 31) & ~31) / 8
		* pbmi->bmiHeader.biHeight;
	// Set biClrImportant to 0, indicating that all of the  
	// device colors are important.  
	pbmi->bmiHeader.biClrImportant = 0;
	return pbmi;
}

// tạo file bitmap
void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi,
	HBITMAP hBMP, HDC hDC)
{
	HANDLE hf;                 // file handle  
	BITMAPFILEHEADER hdr;       // bitmap file-header  
	PBITMAPINFOHEADER pbih;     // bitmap info-header  
	LPBYTE lpBits;              // memory pointer  
	DWORD dwTotal;              // total count of bytes  
	DWORD cb;                   // incremental count of bytes  
	BYTE *hp;                   // byte pointer  
	DWORD dwTmp;

	pbih = (PBITMAPINFOHEADER)pbi;
	lpBits = (LPBYTE)GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);

	if (!lpBits)
		MessageBox(hwnd, L"GlobalAlloc", L"Error", MB_OK);

	// Retrieve the color table (RGBQUAD array) and the bits  
	// (array of palette indices) from the DIB.  
	if (!GetDIBits(hDC, hBMP, 0, (WORD)pbih->biHeight, lpBits, pbi,
		DIB_RGB_COLORS))
	{
		MessageBox(hwnd, L"GetDIBits", L"Error", MB_OK);
	}

	// Create the .BMP file.  
	hf = CreateFile(pszFile,
		GENERIC_READ | GENERIC_WRITE,
		(DWORD)0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		(HANDLE)NULL);
	if (hf == INVALID_HANDLE_VALUE)
		MessageBox(hwnd, L"CreateFile", L"Error", MB_OK);
	hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"  
								// Compute the size of the entire file.  
	hdr.bfSize = (DWORD)(sizeof(BITMAPFILEHEADER) +
		pbih->biSize + pbih->biClrUsed
		* sizeof(RGBQUAD) + pbih->biSizeImage);
	hdr.bfReserved1 = 0;
	hdr.bfReserved2 = 0;

	// Compute the offset to the array of color indices.  
	hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) +
		pbih->biSize + pbih->biClrUsed
		* sizeof(RGBQUAD);

	// Copy the BITMAPFILEHEADER into the .BMP file.  
	if (!WriteFile(hf, (LPVOID)&hdr, sizeof(BITMAPFILEHEADER),
		(LPDWORD)&dwTmp, NULL))
	{
		MessageBox(hwnd, L"WriteFile", L"Error", MB_OK);
	}

	// Copy the BITMAPINFOHEADER and RGBQUAD array into the file.  
	if (!WriteFile(hf, (LPVOID)pbih, sizeof(BITMAPINFOHEADER)
		+ pbih->biClrUsed * sizeof(RGBQUAD),
		(LPDWORD)&dwTmp, (NULL)))
		MessageBox(hwnd, L"WriteFile", L"Error", MB_OK);

	// Copy the array of color indices into the .BMP file.  
	dwTotal = cb = pbih->biSizeImage;
	hp = lpBits;
	if (!WriteFile(hf, (LPSTR)hp, (int)cb, (LPDWORD)&dwTmp, NULL))
		MessageBox(hwnd, L"WriteFile", L"Error", MB_OK);

	// Close the .BMP file.  
	if (!CloseHandle(hf))
		MessageBox(hwnd, L"CloseHandle", L"Error", MB_OK);

	// Free memory.  
	GlobalFree((HGLOBAL)lpBits);
}