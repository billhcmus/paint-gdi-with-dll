#pragma once
#ifdef MYDLL_EXPORTS
#define PAINTLIBRARY_API __declspec(dllexport) 
#else
#define PAINTLIBRARY_API __declspec(dllimport) 
#endif

#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib, "Gdiplus.lib")
using namespace Gdiplus;

namespace PaintLibrary
{
	class CShape
	{
	protected:
		Point StartPos, EndPos;
		Pen *pen;
		Brush *brush;
	public:
		PAINTLIBRARY_API void SetStartPos(Point);
		PAINTLIBRARY_API void SetEndPos(Point);
		PAINTLIBRARY_API void SetSizePen(int);
		PAINTLIBRARY_API void SetPenColor(DWORD);
		PAINTLIBRARY_API virtual void Draw(Graphics*) = 0;
		PAINTLIBRARY_API virtual CShape* Clone() = 0;
		PAINTLIBRARY_API virtual void CreatePen() = 0;
		PAINTLIBRARY_API virtual void CreateBrush() = 0;
	};


	class CLine : public CShape
	{
	public:
		PAINTLIBRARY_API virtual void Draw(Graphics *);
		PAINTLIBRARY_API CShape* Clone();
		PAINTLIBRARY_API void CreatePen();
		PAINTLIBRARY_API void CreateBrush();
	};

	class CBMP : public CShape
	{
	private:
		Image* im;
		Point point;
	public:
		PAINTLIBRARY_API void Draw(Graphics*);
		PAINTLIBRARY_API CShape* Clone();
		PAINTLIBRARY_API void Init(Image*, Point);
		PAINTLIBRARY_API void CreatePen();
		PAINTLIBRARY_API void CreateBrush();
	};

	class CRectangle : public CShape
	{
	public:
		PAINTLIBRARY_API virtual void Draw(Graphics*);
		PAINTLIBRARY_API CShape* Clone();
		PAINTLIBRARY_API void CreatePen();
		PAINTLIBRARY_API void CreateBrush();
	};

	class CEllipse : public CShape
	{
	public:
		PAINTLIBRARY_API virtual void Draw(Graphics*);
		PAINTLIBRARY_API CShape* Clone();
		PAINTLIBRARY_API void CreatePen();
		PAINTLIBRARY_API void CreateBrush();
	};
}

// runtime
extern "C" PAINTLIBRARY_API PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp);
extern "C" PAINTLIBRARY_API void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, HBITMAP hBMP, HDC hDC);
