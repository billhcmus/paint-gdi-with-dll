// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define ID_CMD_NEW 2 
#define ID_CMD_NEW_LabelTitle_RESID 60001
#define ID_CMD_NEW_LargeImages_RESID 60002
#define ID_CMD_OPEN 3 
#define ID_CMD_OPEN_LabelTitle_RESID 60003
#define ID_CMD_OPEN_LargeImages_RESID 60004
#define ID_CMD_SAVE 4 
#define ID_CMD_SAVE_LabelTitle_RESID 60005
#define ID_CMD_SAVE_LargeImages_RESID 60006
#define ID_CMD_EXIT 5 
#define ID_CMD_EXIT_LabelTitle_RESID 60007
#define ID_CMD_EXIT_LargeImages_RESID 60008
#define shapeTab 6 
#define shapeTab_LabelTitle_RESID 60009
#define cmdGroupShapes 7 
#define cmdGroupShapes_LabelTitle_RESID 60010
#define ID_CMD_LINE 8 
#define ID_CMD_LINE_LabelTitle_RESID 60011
#define ID_CMD_LINE_LargeImages_RESID 60012
#define ID_CMD_RECT 9 
#define ID_CMD_RECT_LabelTitle_RESID 60013
#define ID_CMD_RECT_LargeImages_RESID 60014
#define ID_CMD_ELLIPSE 10 
#define ID_CMD_ELLIPSE_LabelTitle_RESID 60015
#define ID_CMD_ELLIPSE_LargeImages_RESID 60016
#define cmdGroupSize 11 
#define cmdGroupSize_LabelTitle_RESID 60017
#define cmdDropDownButton 12 
#define cmdDropDownButton_LabelTitle_RESID 60018
#define cmdDropDownButton_LargeImages_RESID 60019
#define ID_CMD_1PX 13 
#define ID_CMD_1PX_LabelTitle_RESID 60020
#define ID_CMD_1PX_LargeImages_RESID 60021
#define ID_CMD_3PX 14 
#define ID_CMD_3PX_LabelTitle_RESID 60022
#define ID_CMD_3PX_LargeImages_RESID 60023
#define ID_CMD_5PX 15 
#define ID_CMD_5PX_LabelTitle_RESID 60024
#define ID_CMD_5PX_LargeImages_RESID 60025
#define ID_CMD_8PX 16 
#define ID_CMD_8PX_LabelTitle_RESID 60026
#define ID_CMD_8PX_LargeImages_RESID 60027
#define cmdGroupColor 17 
#define cmdGroupColor_LabelTitle_RESID 60028
#define ID_CMD_CHOOSE_COLOR 18 
#define ID_CMD_CHOOSE_COLOR_LabelTitle_RESID 60029
#define ID_CMD_CHOOSE_COLOR_LargeImages_RESID 60030
#define fileMenu_LabelTitle_RESID 60031
