﻿// Paint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Paint.h"
#include "DLL.h"
#include "Commdlg.h"
#include <fstream>
#include <windowsx.h>
#include <vector>
#include <gdiplusimagecodec.h>
#include <Objbase.h>
#include "RibbonFramework.h"
#include "RibbonIDs.h"

#pragma comment(lib, "Ole32.lib")


#define MAX_LOADSTRING 100
#define LINE 0
#define RECTANGLE 1
#define ELLIPSE 2

using namespace std;

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		return FALSE;
	}
	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_PAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PAINT));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	CoUninitialize();
	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0; //CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PAINT);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

using PaintLibrary::CShape;
using PaintLibrary::CLine;
using PaintLibrary::CBMP;
using PaintLibrary::CRectangle;
using PaintLibrary::CEllipse;
HMENU hMenu; // Menu

POINT point;
int x, y; // tọa độ con trỏ chuột
bool isDrawing = false; // đang vẽ hay không
Point StartPos, EndPos, CurPos; // điểm bắt đầu, điểm kết thúc, điểm hiện tại của con trỏ chuột
//CShape* shape;
vector<CShape*> shapes; // mảng chứa shape

// Tạo mẫu đối tượng
vector<CShape*> ShapePrototypes;
int shapeType = -1;

// Memory DC
HDC hdcMem;
HBITMAP hbmMem;
HANDLE hOld;

// Save, Open
OPENFILENAME ofn;
OPENFILENAME sfn;
const int BUFFER_SIZE = 255;
WCHAR szFileName[BUFFER_SIZE] = L"";
Graphics* graphics;
int lineWidth = 1;
DWORD rgb = 0;

// Con trỏ hàm cho load runtime
typedef PBITMAPINFO(__cdecl *ProcBMPStruct)(HWND hwnd, HBITMAP hBmp);
typedef void(__cdecl *ProcBMPFile)(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, HBITMAP hBMP, HDC hDC);
/*
0: Line
1: Rectangle
2: Ellipse
*/
// tính toán tọa độ y để tạo hình vuông, tròn
/*

A * * * * C
   *    *
	 *  *
	    * B
A(startpos.x, startpos.y)
B(endpos.x, endpos.y)
C(endpos.x, startpos.y)
y : endpos.y thay đổi sao cho AC = CB
*/
int CalcY(Point startpos, Point endpos)
{
	int y;
	if (endpos.X > startpos.X)
	{
		if (endpos.Y > startpos.Y)
		{
			y = endpos.X - startpos.X + startpos.Y;
		}
		else
		{
			y = startpos.X + startpos.Y - endpos.X;
		}
	}
	else
	{
		if (endpos.Y > startpos.Y)
		{
			y = startpos.X - endpos.X + startpos.Y;
		}
		else
		{
			y = startpos.Y - startpos.X + endpos.X;
		}
	}
	return y;
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	Gdiplus::GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	Gdiplus::GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}


GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR gdiplusToken;
bool initSuccess;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	hMenu = GetMenu(hWnd);

	switch (message)
	{
	case WM_CREATE:
	{
		// Init ribbon framework
		initSuccess = InitializeFramework(hWnd);
		if (!initSuccess)
		{
			return -1;
		}
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		ShapePrototypes.push_back(new CLine);
		ShapePrototypes.push_back(new CRectangle);
		ShapePrototypes.push_back(new CEllipse);
		break;
	}
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_CMD_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_CMD_NEW:
			for (int i = shapes.size() - 1; i >= 0; i--)
			{
				delete shapes[i];
				shapes.pop_back();
			}
			InvalidateRect(hWnd, NULL, FALSE);
			break;
		case ID_CMD_OPEN:
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hWnd;
			ofn.nMaxFile = BUFFER_SIZE;
			ofn.lpstrFilter = L"BMP (*.bmp) \0*.bmp\0";
			ofn.lpstrFile = szFileName;
			ofn.nFilterIndex = 1;
			ofn.lpstrInitialDir = NULL;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
			if(GetOpenFileName(&ofn) == TRUE)
			{
				Image* img = Image::FromFile(ofn.lpstrFile);
				Point p;
				p.X = 0;
				p.Y = GetRibbonHeight();
				CBMP* bmpImg = new CBMP;
				bmpImg->Init(img, p);
				for (int i = shapes.size() - 1; i >= 0; i--)
				{
					delete shapes[i];
					shapes.pop_back();
				}
				shapes.push_back(bmpImg);
				InvalidateRect(hWnd, NULL, FALSE);
			}
			break;
		case ID_CMD_SAVE: // lưu file bitmap
			// Load Library
			HINSTANCE inst;
			ProcBMPStruct ProcCreateBMPInfoStruct;
			ProcBMPFile ProcCreateBMPFile;
			ProcCreateBMPInfoStruct = NULL;
			ProcCreateBMPFile = NULL;
			inst = LoadLibrary(L"MyDLL.dll");
			if (inst)
			{
				ProcCreateBMPInfoStruct = (ProcBMPStruct)GetProcAddress(inst, "CreateBitmapInfoStruct");
				ProcCreateBMPFile = (ProcBMPFile)GetProcAddress(inst, "CreateBMPFile");
			}
			else
			{
				MessageBox(0, L"Error load MyDLL.dll file", 0, 0);
			}


			DWORD fileAttr;
			fileAttr = GetFileAttributes(szFileName);
			if (fileAttr == 0xFFFFFFFF) { // Chưa có file nào có sẵn
				ZeroMemory(&sfn, sizeof(sfn)); 
				sfn.lStructSize = sizeof(sfn);
				sfn.hwndOwner = hWnd;
				sfn.nMaxFile = BUFFER_SIZE;
				sfn.lpstrFilter = L"BMP (*.bmp) \0*.bmp\0";
				sfn.lpstrFile = szFileName;
				sfn.nFilterIndex = 1;
				sfn.lpstrInitialDir = NULL;
				sfn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
				sfn.lpstrDefExt = L"File";
				if (GetSaveFileName(&sfn)) // lấy tên file
				{
					HDC hdc = CreateCompatibleDC(GetDC(hWnd));

					RECT rect;
					GetClientRect(hWnd, &rect);
					HBITMAP hbmScreen = CreateCompatibleBitmap(GetDC(hWnd), rect.right, rect.bottom);
					SelectObject(hdc, hbmScreen);
					FillRect(hdc, &rect, (HBRUSH)(COLOR_WINDOW + 1));
					BitBlt(hdc, 0, GetRibbonHeight(), rect.right, rect.bottom, GetDC(hWnd), 0, GetRibbonHeight(), SRCCOPY);


					PBITMAPINFO pbi = (ProcCreateBMPInfoStruct)(hWnd, hbmScreen);
					(ProcCreateBMPFile)(hWnd, sfn.lpstrFile, pbi, hbmScreen, GetDC(hWnd));
				}
			}
			else
			{
				HDC hdc = CreateCompatibleDC(GetDC(hWnd));

				RECT rect;
				GetClientRect(hWnd, &rect);
				HBITMAP hbmScreen = CreateCompatibleBitmap(GetDC(hWnd), rect.right, rect.bottom);
				SelectObject(hdc, hbmScreen);
				FillRect(hdc, &rect, (HBRUSH)(COLOR_WINDOW + 1));
				BitBlt(hdc, 0, 0, rect.right, rect.bottom, GetDC(hWnd), 0, 0, SRCCOPY);

				PBITMAPINFO pbi = (ProcCreateBMPInfoStruct)(hWnd, hbmScreen);
				(ProcCreateBMPFile)(hWnd, szFileName, pbi, hbmScreen, GetDC(hWnd));
			}

			
			break;
		case ID_CMD_LINE:
			//CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);

			//// uncheck mục còn lại
			//CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			//CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
			shapeType = LINE;
			break;
		case ID_CMD_RECT:
			/*CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_CHECKED);

			CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);
			CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);*/
			shapeType = RECTANGLE;
			break;
		case ID_CMD_ELLIPSE:
			/*CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_CHECKED);

			CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);*/
			shapeType = ELLIPSE;
			break;
		case ID_CMD_1PX:
			lineWidth = 1;
			break;
		case ID_CMD_3PX:
			lineWidth = 3;
			break;
		case ID_CMD_5PX:
			lineWidth = 5;
			break;
		case ID_CMD_8PX:
			lineWidth = 8;
			break;
		case ID_CMD_CHOOSE_COLOR:
			CHOOSECOLOR cc;                 // common dialog box structure 
			static COLORREF acrCustClr[16]; // array of custom colors 
											// owner window
			HBRUSH hbrush;                  // brush handle
			static DWORD rgbCurrent;        // initial color selection

											// Initialize CHOOSECOLOR 
			ZeroMemory(&cc, sizeof(cc));
			cc.lStructSize = sizeof(cc);
			cc.hwndOwner = hWnd;
			cc.lpCustColors = (LPDWORD)acrCustClr;
			cc.rgbResult = rgbCurrent;
			cc.Flags = CC_FULLOPEN | CC_RGBINIT;

			if (ChooseColor(&cc) == TRUE)
			{
				hbrush = CreateSolidBrush(cc.rgbResult);
				rgbCurrent = cc.rgbResult;
				rgb = rgbCurrent;
			}

			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		if (!isDrawing && shapeType != -1)
		{
			shapes.push_back(ShapePrototypes[shapeType]->Clone());
			isDrawing = true;
			StartPos.X = x;
			StartPos.Y = y;
			shapes[shapes.size() - 1]->SetStartPos(StartPos);
			shapes[shapes.size() - 1]->CreatePen();
			shapes[shapes.size() - 1]->SetSizePen(lineWidth);
			shapes[shapes.size() - 1]->SetPenColor(rgb);
		//	HBRUSH hbrush = GetStockBrush(NULL_BRUSH);
		//	shapes[shapes.size() - 1]->SetBrush(hbrush);
			SetCapture(hWnd); // cho chuot ra khoi cua so
		}
		break;
	}
	break;
	case WM_MOUSEMOVE:
	{
		if (isDrawing == true)
		{
			x = GET_X_LPARAM(lParam);
			y = GET_Y_LPARAM(lParam);
			CurPos.X = x, CurPos.Y = y;
			TCHAR buffer[10];
			wsprintf(buffer, L"%d, %d", x, y);
			SetWindowText(hWnd, buffer);
			if (MK_SHIFT&wParam && (shapeType == RECTANGLE || shapeType == ELLIPSE)) // nếu nhấn shift và đó là hình chữ nhật hoặc hình ellipse
			{
				CurPos.Y = CalcY(StartPos, CurPos);
			}
			int index = shapes.size();
			shapes[index - 1]->SetEndPos(CurPos);
			InvalidateRect(hWnd, NULL, FALSE);
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		if (isDrawing == true)
		{
			isDrawing = false; // hết vẽ
							   // Lấy điểm kết thúc
			EndPos.X = GET_X_LPARAM(lParam);
			EndPos.Y = GET_Y_LPARAM(lParam);
			if (MK_SHIFT&wParam && (shapeType == RECTANGLE || shapeType == ELLIPSE))
			{
				EndPos.Y = CalcY(StartPos, EndPos);
			}
			shapes[shapes.size() - 1]->SetEndPos(EndPos);
			ReleaseCapture();
			InvalidateRect(hWnd, NULL, FALSE);
		}
		break;
	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		RECT rect;
		HDC hdc = BeginPaint(hWnd, &ps);
		HPALETTE hPalette;
		BITMAP bm;

		GetClientRect(hWnd, &rect);
		// TODO: Add any drawing code that uses hdc here...
		hdcMem = CreateCompatibleDC(hdc);
		hbmMem = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
		hOld = SelectObject(hdcMem, hbmMem); // lưu lại
		FillRect(hdcMem, &rect, (HBRUSH)(COLOR_WINDOW + 1));
		graphics = new Graphics(hdcMem);
		for (int i = 0; i < shapes.size(); i++)
		{
			shapes[i]->Draw(graphics);
		}

		BitBlt(hdc, 0, GetRibbonHeight(), rect.right, rect.bottom, hdcMem, 0,  GetRibbonHeight(), SRCCOPY);
		SelectObject(hdcMem, hOld); // lấy lên
		DeleteObject(hbmMem); // giải phóng
		DeleteDC(hdcMem);
		EndPaint(hWnd, &ps);
	}
	break;

	case WM_DESTROY:
		GdiplusShutdown(gdiplusToken);
		DestroyFramework();
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
